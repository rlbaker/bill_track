from bill_track.settings.base import *  # noqa

SECRET_KEY = 'dev'

DEBUG = True

# IP's which are allowed to view the django-debug-toolbar
INTERNAL_IPS += ["127.0.0.1"]

# Application definition
INSTALLED_APPS += [
    'debug_toolbar',
]

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'dev_db.sqlite3'),
    }
}

# Local directory in which static files will be collected and served
STATIC_ROOT = './static/'
