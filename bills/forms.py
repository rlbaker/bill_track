from django import forms
from django.utils import timezone

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, ButtonHolder, Submit

from bills.models import Bill


class CustomFormHelper(FormHelper):
    form_tag = False


class BillCreateForm(forms.ModelForm):
    class Meta:
        model = Bill
        fields = ('name', 'due_date')
        widgets = {'name': forms.TextInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = 'bill-create-form'
        self.helper.form_class = "col-md-8 offset-md-2 col-lg-6 offset-lg-3"

        self.helper.layout = Layout(
            Fieldset(
                'Create Bill',
                Field('name', placeholder="Car Insurance"),
                Field(
                    'due_date',
                    placeholder=timezone.now().strftime("%Y-%m-%d"),
                    data_date_start_date=timezone.now().strftime("%Y-%m-%d"),
                    data_date_format="yyyy-mm-dd",
                    data_provide="datepicker"
                ),
            ),
            ButtonHolder(
                Submit('submit', 'Save Bill')
            )
        )
