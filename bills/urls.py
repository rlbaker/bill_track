from django.conf.urls import url

from bills import views

urlpatterns = [
    url(r'^$', views.UnpaidBillListView.as_view(), name='unpaid-bill-list'),
    url(r'^all/$', views.BillListView.as_view(), name='bill-list'),
    url(r'^detail/(?P<slug>[^/]+)$', views.BillDetailView.as_view(), name='bill-detail'),
    url(r'^create$', views.BillCreateView.as_view(), name='bill-create'),
]
