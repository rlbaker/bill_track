from django.contrib.auth.models import User
from django.test import Client, TransactionTestCase
from django.urls import reverse

# from bills.models import Bill


class BillTestCase(TransactionTestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='user1', email='user1@example.com', password='user1password')
        self.other_user = User.objects.create_user(username='user2', email='user2@example.com', password='user2password')

    def test_user_can_login(self):
        client = Client()
        response = client.post(reverse('auth_login'),
                               {'username': 'user1', 'password': 'user1password'},
                               follow=True)
        self.assertRedirects(response, reverse('unpaid-bill-list'))

    def test_user_can_logout(self):
        client = Client()
        client.login(username='user1', password='user1password')

        response = client.get(reverse('auth_logout'))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse('unpaid-bill-list'))
        self.assertRedirects(response, '/accounts/login/?next=/bills/')

    def test_user_can_create_bill(self):
        client = Client()
        client.login(username='user1', password='user1password')

        response = client.post(reverse('bill-create'),
                               {'name': 'new bill', 'due_date': '2017-01-01'},
                               follow=True)

        bill = self.user.bill_set.filter(name='new bill').first()

        self.assertRedirects(response, '/bills/detail/' + str(bill.slug))

    def test_user_can_view_own_bills(self):
        self.user.bill_set.create(name="bill1", due_date="2017-01-01", is_paid=False)
        self.user.bill_set.create(name="bill2", due_date="2017-01-02", is_paid=False)

        client = Client()
        client.login(username='user1', password='user1password')
        response = client.get(reverse('unpaid-bill-list'))

        self.assertContains(response, 'bill1')
        self.assertContains(response, 'bill2')

    def test_user_can_view_details_of_own_bills(self):
        bill = self.user.bill_set.create(name="detail bill", due_date="2017-01-01", is_paid=False)

        client = Client()
        client.login(username='user1', password='user1password')

        response = client.get(reverse('bill-detail', kwargs={'slug': str(bill.slug)}), follow=False)

        self.assertContains(response, 'detail bill')

    def test_user_cant_view_others_bills(self):
        self.other_user.bill_set.create(name="bill1", due_date="2017-01-01", is_paid=False)
        self.other_user.bill_set.create(name="bill2", due_date="2017-01-02", is_paid=False)

        client = Client()
        client.login(username='user1', password='user1password')
        response = client.get(reverse('unpaid-bill-list'))

        self.assertNotContains(response, 'bill1')
        self.assertNotContains(response, 'bill2')

    def test_user_cant_view_details_of_others_bills(self):
        bill = self.other_user.bill_set.create(name="detail bill", due_date="2017-01-01", is_paid=False)

        client = Client()
        client.login(username='user1', password='user1password')

        response = client.get(reverse('bill-detail', kwargs={'slug': str(bill.slug)}), follow=True)

        self.assertEquals(response.status_code, 404)
